<?php

use Illuminate\Database\Seeder;

class ImagensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artigos_imagem')->insert(['imagem' => '']);
        DB::table('links_imagem')->insert(['imagem' => '']);
        DB::table('parceiros_imagem')->insert(['imagem' => '']);
    }
}
