<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParceirosTable extends Migration
{
    public function up()
    {
        Schema::create('parceiros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('link');
            $table->timestamps();
        });

        Schema::create('parceiros_imagem', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('parceiros');
        Schema::drop('parceiros_imagem');
    }
}
