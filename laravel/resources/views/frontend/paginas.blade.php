@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="imagem" style="background-image:url('{{ asset('assets/img/paginas/'.$pagina->imagem) }}')"></div>

        <div class="texto">
            <h1>{{ $pagina->titulo }}</h1>
            {!! $pagina->texto !!}
        </div>
    </div>

@endsection
