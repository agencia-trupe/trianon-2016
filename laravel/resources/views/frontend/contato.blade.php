@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="imagem" style="background-image:url('{{ asset('assets/img/contato/'.$contato->imagem) }}')"></div>

        <div class="texto contato">
            <h1>CONTATO</h1>

            <div class="imagens">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem_secundaria_1) }}" alt="">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem_secundaria_2) }}" alt="">
            </div>

            <div class="informacoes">
                <p>
                    Esta é a Cláudia!<br>
                    Ela pode ajudá-lo a esclarecer dúvidas relativas aos tratamentos, procedimentos relativos ao Projeto Escuta, agendamento de consultas, etc.
                </p>
                <p>
                    <strong>FALE COM ELA!</strong><br>
                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                </p>
                <p class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <span>{{ $telefone }}</span>
                    @endforeach
                </p>
            </div>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div id="form-contato-response"></div>
            </form>

            <div class="googlemaps">
                <h2>LOCALIZAÇÃO</h2>
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
                {!! $contato->google_maps !!}
            </div>
        </div>
    </div>

@endsection
