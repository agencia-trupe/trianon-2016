@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="imagem" style="background-image:url('{{ asset('assets/img/artigos/'.$artigosImagem->imagem) }}')"></div>

        <div class="texto">
            <h1>ARTIGOS</h1>

            <div class="artigos-links">
            @foreach($artigos as $artigo)
                <a href="{{ url('assets/artigos/'.$artigo->arquivo) }}" target="_blank">
                    {{ $artigo->titulo }}
                </a>
            @endforeach
            </div>

            {!! $artigos->render() !!}
        </div>
    </div>

@endsection
