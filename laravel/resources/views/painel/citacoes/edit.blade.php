@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Citações /</small> Editar Citação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.citacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.citacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
