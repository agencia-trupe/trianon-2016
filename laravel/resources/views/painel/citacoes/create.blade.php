@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Citações /</small> Adicionar Citação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.citacoes.store', 'files' => true]) !!}

        @include('painel.citacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
