@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Artigos /</small> Editar Artigo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.artigos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.artigos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
