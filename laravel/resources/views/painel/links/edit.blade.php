@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Links /</small> Editar Link</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.links.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.links.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
