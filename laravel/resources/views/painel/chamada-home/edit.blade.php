@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Chamada Home</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.chamada-home.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.chamada-home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
