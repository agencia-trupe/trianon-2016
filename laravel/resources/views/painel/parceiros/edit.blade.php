@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Parceiros /</small> Editar Parceiro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.parceiros.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.parceiros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
