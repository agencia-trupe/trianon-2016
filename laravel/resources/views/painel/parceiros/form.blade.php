@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional, incluir prefixo http://)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.parceiros.index') }}" class="btn btn-default btn-voltar">Voltar</a>
