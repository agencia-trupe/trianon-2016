@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Parceiros
            <a href="{{ route('painel.parceiros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Parceiro</a>
        </h2>
    </legend>

    <div class="row">
        <div class="col-md-9">
            @if(!count($registros))
            <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
            @else
            <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="parceiros">
                <thead>
                    <tr>
                        <th>Ordenar</th>
                        <th>Nome</th>
                        <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($registros as $registro)
                    <tr class="tr-row" id="{{ $registro->id }}">
                        <td>
                            <a href="#" class="btn btn-info btn-sm btn-move">
                                <span class="glyphicon glyphicon-move"></span>
                            </a>
                        </td>
                        <td>{{ $registro->nome }}</td>
                        <td class="crud-actions">
                            {!! Form::open([
                                'route'  => ['painel.parceiros.destroy', $registro->id],
                                'method' => 'delete'
                            ]) !!}

                            <div class="btn-group btn-group-sm">
                                <a href="{{ route('painel.parceiros.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                                    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                                </a>

                                <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                            </div>

                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <div class="col-md-3">
            <div class="well form-group">
                {!! Form::open(['route' => 'painel.parceiros.imagem', 'files' => true]) !!}
                    {!! Form::label('imagem', 'Imagem') !!}
                    <img src="{{ url('assets/img/parceiros/'.$parceirosImagem->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
                    {!! Form::file('imagem', ['class' => 'form-control']) !!}
                    {!! Form::submit('Alterar', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top:10px']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
