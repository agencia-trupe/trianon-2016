namespace App\Http\Requests;

use App\Http\Requests\Request;

class {{ $request }} extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
@foreach($gen->fields as $field)
            '{{ $field['name'] }}' => '{{ $field['validation'] }}',
@endforeach
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
