namespace App\Http\Controllers\{{ $gen->namespace }};

use Illuminate\Http\Request;

use App\Http\Requests\{{ $requestName }};
use App\Http\Controllers\Controller;

use App\Models\{{ $gen->model }};

class {{ $controllerName }} extends Controller
{
    public function index()
    {
        $registros = {{ $gen->model }}::{!! $indexMethod !!};

        return view('{{ $namespace }}.{{ $route }}.index', compact('registros'));
    }

    public function create()
    {
        return view('{{ $namespace }}.{{ $route }}.create');
    }

    public function store({{ $requestName }} $request)
    {
        try {

            $input = $request->all();

            {{ $gen->model }}::create($input);
            return redirect()->route('{{ $namespace }}.{{ $route }}.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit({{ $gen->model }} $registro)
    {
        return view('{{ $namespace }}.{{ $route }}.edit', compact('registro'));
    }

    public function update({{ $requestName }} $request, {{ $gen->model }} $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('{{ $namespace }}.{{ $route }}.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy({{ $gen->model }} $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('{{ $namespace }}.{{ $route }}.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
