<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Contato extends Model
{
    protected $table = 'contato';

    protected $guarded = ['id'];

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 980,
            'height' => null,
            'path'   => 'assets/img/contato/'
        ]);
    }

    public static function uploadImagemSecundaria($numero)
    {
        return CropImage::make('imagem_secundaria_'.$numero, [
            'width'  => 395,
            'height' => 225,
            'path'   => 'assets/img/contato/'
        ]);
    }
}
