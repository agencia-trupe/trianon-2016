<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\CropImage;

class ArtigosImagem extends Model
{
    protected $table = 'artigos_imagem';

    protected $guarded = ['id'];

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 980,
            'height' => null,
            'path'   => 'assets/img/artigos/'
        ]);
    }
}
