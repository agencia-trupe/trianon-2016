<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Parceiro;
use App\Models\ParceirosImagem;

class ParceirosController extends Controller
{
    public function index()
    {
        $parceiros = Parceiro::ordenados()->get();
        $parceirosImagem = ParceirosImagem::first();

        return view('frontend.parceiros', compact('parceiros', 'parceirosImagem'));
    }
}
