<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContatoRequest;
use App\Http\Controllers\Controller;

use App\Models\Contato;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('painel.contato.index', compact('contato'));
    }

    public function update(ContatoRequest $request, Contato $contato)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Contato::uploadImagem();
            if (isset($input['imagem_secundaria_1'])) $input['imagem_secundaria_1'] = Contato::uploadImagemSecundaria(1);
            if (isset($input['imagem_secundaria_2'])) $input['imagem_secundaria_2'] = Contato::uploadImagemSecundaria(2);

            $contato->update($input);
            return redirect()->route('painel.contato.index')->with('success', 'Informações alteradas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: '.$e->getMessage()]);

        }
    }
}
