<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CitacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Citacao;

class CitacoesController extends Controller
{
    public function index()
    {
        $registros = Citacao::ordenados()->get();

        return view('painel.citacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.citacoes.create');
    }

    public function store(CitacoesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Citacao::uploadImagem();

            Citacao::create($input);
            return redirect()->route('painel.citacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Citacao $registro)
    {
        return view('painel.citacoes.edit', compact('registro'));
    }

    public function update(CitacoesRequest $request, Citacao $registro)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = Citacao::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.citacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Citacao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.citacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
