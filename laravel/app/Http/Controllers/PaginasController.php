<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Pagina;

class PaginasController extends Controller
{
    public function index(Pagina $pagina)
    {
        return view('frontend.paginas', compact('pagina'));
    }
}
