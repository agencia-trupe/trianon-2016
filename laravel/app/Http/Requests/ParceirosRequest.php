<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ParceirosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
