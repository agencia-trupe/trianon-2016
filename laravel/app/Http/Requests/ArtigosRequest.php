<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArtigosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
