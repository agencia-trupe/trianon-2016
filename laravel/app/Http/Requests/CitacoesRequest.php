<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CitacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'frase' => 'required',
            'autor' => 'required',
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
